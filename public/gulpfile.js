var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();
var autoprefixer = require('gulp-autoprefixer');
var cleanCSS = require('gulp-clean-css');
var rename = require('gulp-rename');

gulp.task('serve', ['sass'], function() {
  // place code for your default task here

  browserSync.init({
    proxy: "http://localhost"
  });
  gulp.watch('scss/**/*.scss', ['sass']);
  gulp.watch("index.html").on("change", browserSync.reload);
  //gulp.watch("**/*.twig", ['sass']);
  //gulp.watch("templates/*.twig").on("change", browserSync.reload);
  //gulp.watch("js/main.js").on("change", browserSync.reload);
});

gulp.task('sass', function() {
  return gulp.src('scss/style.scss')
    .pipe(sass()) // Converts Sass to CSS with gulp-sass
    .pipe(autoprefixer())
    .pipe(cleanCSS())
    .pipe(gulp.dest(''))
    .pipe(browserSync.stream());
});

gulp.task('default', ['serve']);
